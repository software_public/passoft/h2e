#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Tests for `h2e` package."""

import io
import unittest

from contextlib import redirect_stdout
from unittest.mock import patch
from h2e.h2e import get_date, main, is_date_valid

VALID_DATE = '2020:202:14:46:30'
UNVALID_DATE = '2020:370:14:46:30'


class TestH2e(unittest.TestCase):
    """Tests for `h2e` package."""

    def test_is_date_valid(self):
        """Test basic functionality of is_date_valid"""

        date = is_date_valid(VALID_DATE)

        # prints this message if the function returns False
        error_message = "Function returned False. The date is not valid."

        # tests if user input equals True or False
        self.assertTrue(date, error_message)

    @patch('h2e.h2e.sys.exit', autospec=True)
    def test_get_date(self, mock_exit):
        """Test basic functionality of get_date"""
        date = get_date(VALID_DATE)
        expected_month = 7
        expected_day = 20
        self.assertEqual(date.month, expected_month, "Date formating failed! "
                         "Month expected: {} - Month returned: {}"
                         .format(expected_month, date.month))
        self.assertEqual(date.day, expected_day, "Date formating failed! "
                         "Day expected: {} - Day returned: {}"
                         .format(expected_day, date.day))
        get_date(UNVALID_DATE)
        self.assertTrue(mock_exit.called, "sys.exit(1) never called - "
                        "get_month_day() not fully exercised!")

    def test_h2e(self):
        """Test basic functionality of h2e"""
        with patch('sys.argv', ['h2e']):
            with self.assertRaises(SystemExit) as cmd:
                main()
            self.assertEqual(cmd.exception.code, 1, "sys.exit(1) never called "
                             "- h2e not fully exercised!")
        with patch('sys.argv', ['h2e ', VALID_DATE]):
            stdout = io.StringIO()
            with redirect_stdout(stdout):
                main()
            epoch = stdout.getvalue().strip()
            expected_epoch = "1595256390"
            self.assertEqual(epoch, expected_epoch, "Failed to properly "
                             "convert date!")
