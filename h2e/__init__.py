# -*- coding: utf-8 -*-

"""Top-level package for h2e."""

__author__ = """IRIS PASSCAL"""
__email__ = 'software-support@passcal.nmt.edu'
__version__ = '2023.2.0.0'
