#!/usr/bin/env python

"""
h2e.py given a calendar date will convert the date into its epoch.

Maeva Pourpoint
July 2020
Updates to work under Python 3.
Unit tests to ensure basic functionality.
Code cleanup to conform to the PEP8 style guide.
Directory cleanup (remove unused files introduced by Cookiecutter).
Packaged with conda.
"""

import datetime
import os
import sys
import time


def is_date_valid(calendar_date):
    """Check if the date of the year is valid."""
    try:
        datetime.datetime.strptime(calendar_date, "%Y:%j:%H:%M:%S")
        return True
    except ValueError:
        return False


def get_date(calendar_date):
    """Return datetime object if the date of the year is valid"""
    if is_date_valid(calendar_date):
        date = datetime.datetime.strptime(calendar_date, "%Y:%j:%H:%M:%S")
        return date
    else:
        print("Invalid date")
        sys.exit(1)


def main():
    """Return epoch for given date"""
    if len(sys.argv) != 2:
        print("USAGE: h2e YYYY:JJJ:HH:MM:SS")
        sys.exit(1)
    os.environ['TZ'] = 'UTC'
    time.tzset()
    date_str = sys.argv[1]
    date = get_date(date_str)
    week_day = datetime.datetime(date.year, date.month, date.day).weekday()
    epoch = time.mktime((date.year, date.month, date.day, date.hour,
                         date.minute, date.second, week_day,
                         date.timetuple().tm_yday, -1))
    print(int(epoch))


if __name__ == "__main__":
    main()
