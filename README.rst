===
h2e
===

* Description: convert human readable time format to epoch.

* Usage: h2e YYYY:JJJ:HH:MM:SS

* Free software: GNU General Public License v3 (GPLv3)
